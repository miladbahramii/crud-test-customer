
import '../error/failures.dart';

const UNIMPLEMENTED_FAILURE_MESSAGE_VARIABLE = 'unimplemented failure message';

abstract class CubitTools {
  String mapFailureToMessage(Failure failure);
}

class CubitToolsImpl implements CubitTools {
  @override
  String mapFailureToMessage(Failure failure) {
    if(failure is UniqueConstrantFailure) {
      return failure.message;
    }
    return UNIMPLEMENTED_FAILURE_MESSAGE_VARIABLE;
  }
}
