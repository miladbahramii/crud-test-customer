import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {}

// General failures
class ServerFailure extends Failure {
  final String statusText;

  ServerFailure(this.statusText);
  @override
  List<Object> get props => [statusText];
}

class UniqueConstrantFailure extends Failure {
  final String message;

  UniqueConstrantFailure(this.message);

  @override
  List<Object> get props => [];
}

class CacheFailure extends Failure {
  @override
  List<Object> get props => [];
}

class NetworkFailure extends Failure {
  @override
  List<Object> get props => [];
}

class UnAuthorizeFailure extends Failure {
  @override
  List<Object> get props => [];
}

class InvalidInputFailure extends Failure {
  @override
  List<Object> get props => [];
}