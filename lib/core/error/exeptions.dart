import 'package:drift/drift.dart';

class ServerException implements Exception {
  final String statusCode;

  ServerException(this.statusCode);
}

class CacheException implements Exception {}
class UniqueConstrantExeption implements Exception {
  final String message;

  UniqueConstrantExeption(this.message);
}
class NetworkException implements Exception {}
class NotFoundException implements Exception {}
class UnAuthorizeException implements Exception {}
class UnExpectedExeption implements Exception {}