import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  final double? strokeWidth;
  final Color? color;
  const LoadingWidget({Key? key, this.strokeWidth, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: CircularProgressIndicator(
          strokeWidth: strokeWidth != null ? strokeWidth! : 3,
          // backgroundColor: Colors.white,
          valueColor: new AlwaysStoppedAnimation<Color?>(color),
        ),
      ),
    );
  }
}
