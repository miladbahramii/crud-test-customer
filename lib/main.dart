import 'package:flutter/material.dart';
import 'package:mc_crud_test/features/customer/presentation/pages/register_page.dart';
import 'injection_container.dart' as di;

Future<void> main() async {
  di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: RegisterPage(),
    );
  }
}
