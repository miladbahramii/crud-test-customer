import 'package:get_it/get_it.dart';
import 'package:mc_crud_test/core/database/database.dart';
import 'package:mc_crud_test/core/validator/validator.dart';
import 'package:mc_crud_test/features/customer/data/datasources/customer_local_data_source.dart';
import 'package:mc_crud_test/features/customer/data/datasources/customer_remote_data_source.dart';
import 'package:mc_crud_test/features/customer/data/models/customer_model.dart';
import 'package:mc_crud_test/features/customer/data/repositories/customer_repository_impl.dart';
import 'package:mc_crud_test/features/customer/domain/repositories/customer_repository.dart';
import 'package:mc_crud_test/features/customer/domain/usecases/create_customer.dart';
import 'package:mc_crud_test/features/customer/presentation/cubit/customer_cubit.dart';

import 'core/utils/cubit_tools.dart';

final sl = GetIt.instance;

void init() {
  //! Features
  customerFeatureInjection();
  //! Core
  sl.registerLazySingleton<CubitTools>(() => CubitToolsImpl());
  sl.registerLazySingleton(() => AppDatabase());
  sl.registerLazySingleton(() => Validator());
}

void customerFeatureInjection() {
  //! Cubit
  sl.registerFactory(() =>
      CustomerCubit(createCustomer: sl(), cubitTools: sl(), validator: sl()));
  //! Usecases
  sl.registerLazySingleton(() => CreateCustomer(sl()));
  //! Repository
  sl.registerLazySingleton<CustomerRepository>(
      () => CustomerRepositoryImpl(localDataSource: sl()));
  //! Datasource
  sl.registerLazySingleton<CustomerLocalDataSource>(
      () => CustomerLocalDataSourceImpl(database: sl()));
  sl.registerLazySingleton<CustomerRemoteDataSource>(
      () => CustomerRemoteDataSourceImpl()); // its empty
}
