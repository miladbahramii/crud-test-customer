part of 'customer_cubit.dart';

abstract class CustomerState extends Equatable {
  const CustomerState();

  @override
  List<Object> get props => [];
}

class CustomerInitial extends CustomerState {}
class Loading extends CustomerState {}
class CustomerCreated extends CustomerState {
  final int quantity;

  CustomerCreated({required this.quantity});
}
class DatabaseError extends CustomerState {
  final String errorMessage;

  DatabaseError(this.errorMessage);
}

class ValidePhone extends CustomerState {
}

class NotValidePhone extends CustomerState {
}

class ValideEmail extends CustomerState {
}

class NotValideEmail extends CustomerState {
}

class ValideAccountNumber extends CustomerState {
}

class NotValideAccountNumber extends CustomerState {
}