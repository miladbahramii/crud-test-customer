import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mc_crud_test/core/utils/cubit_tools.dart';
import 'package:mc_crud_test/core/validator/validator.dart';
import 'package:mc_crud_test/features/customer/domain/usecases/create_customer.dart';

part 'customer_state.dart';

class CustomerCubit extends Cubit<CustomerState> {
  CustomerCubit(
      {required this.createCustomer,
      required this.cubitTools,
      required this.validator})
      : super(CustomerInitial());

  final CreateCustomer createCustomer;
  final CubitTools cubitTools;
  final Validator validator;

  Future<void> createNewCustomer(
      {required String firstname,
      required String lastname,
      required DateTime birthDay,
      required String phone,
      required String email,
      required String bankAccountNumber}) async {
    emit(Loading());
    final unfoldedCustomer = await createCustomer(CreateCustomerParams(
        firstname: firstname,
        lastname: lastname,
        birthDay: birthDay,
        phone: phone,
        email: email,
        bankAccountNumber: bankAccountNumber));
    unfoldedCustomer.fold((failure) {
      final message = cubitTools.mapFailureToMessage(failure);
      emit(DatabaseError(message));
    }, (customerQuantity) {
      emit(CustomerCreated(quantity: customerQuantity));
    });
  }

  Future<void> validatePhone(String phone) async {
    final isValidePhone = validator.isValidePhone(phone);
    if(isValidePhone) {
      emit(ValidePhone());
    }
    else {
      emit(NotValidePhone());
    }
  }

  Future<void> validateEmail(String email) async {
    final isValideEmail = validator.isValideEmail(email);
    if(isValideEmail) {
      emit(ValideEmail());
    }
    else {
      emit(NotValideEmail());
    }
  }

  Future<void> validateAccountNumber(String accountNumber) async {
    final isValideAcc = validator.isValideAccountNumber(accountNumber);
    if(isValideAcc) {
      emit(ValideAccountNumber());
    }
    else {
      emit(NotValideAccountNumber());
    }
  }
}
