import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mc_crud_test/features/customer/presentation/cubit/customer_cubit.dart';
import 'package:mc_crud_test/injection_container.dart';

import '../widgets/register_display.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => sl<CustomerCubit>(),
      child: Scaffold(
        body: RegisterDisplay(),
        appBar: AppBar(title: Text('Register')),
      ),
    );
  }
}
