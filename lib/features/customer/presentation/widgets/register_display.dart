import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mc_crud_test/core/widgets/loading_widget.dart';
import 'package:mc_crud_test/features/customer/presentation/cubit/customer_cubit.dart';

class RegisterDisplay extends StatefulWidget {
  RegisterDisplay({Key? key}) : super(key: key);

  @override
  State<RegisterDisplay> createState() => _RegisterDisplayState();
}

class _RegisterDisplayState extends State<RegisterDisplay> {
  final registerFormKey = GlobalKey<FormState>();

  String firstname = '';
  String lastname = '';
  DateTime birthday = DateTime(1990);
  String phone = '';
  String email = '';
  String bankAccountNumber = '';

  @override
  void initState() {
    super.initState();
    context.read<CustomerCubit>().stream.listen((state) {
      if (state is CustomerCreated) {
        showSnackbar('customer successfully added', true);
      } else if (state is DatabaseError) {
        showSnackbar(state.errorMessage, false);
      }
    });
  }

  void showSnackbar(String message, bool isSuccess) {
    var snackBar = SnackBar(
      backgroundColor: isSuccess ? Colors.green : Colors.red,
      content: Text(message),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _showDatePicker() async {
    final picked = await showDatePicker(
      context: context,
      initialDate: birthday,
      firstDate: DateTime(1900),
      lastDate: DateTime(2020),
      initialEntryMode: DatePickerEntryMode.input,
    );

    if (picked != null && picked != birthday) {
      setState(() {
        birthday = picked;
      });
    }
  }

  void onSubmit() {
    if (registerFormKey.currentState!.validate()) {
      context.read<CustomerCubit>().createNewCustomer(
          firstname: firstname,
          lastname: lastname,
          birthDay: DateTime.now(),
          phone: phone,
          email: email,
          bankAccountNumber: bankAccountNumber);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: SingleChildScrollView(
          child: Form(
        key: registerFormKey,
        child: BlocBuilder<CustomerCubit, CustomerState>(
          builder: (context, state) {
            return Column(children: [
              TextFormField(
                validator: (text) {
                  if(text!.isEmpty) {
                    return 'this field is required';
                  }
                  else {
                    return null;
                  }
                },
                decoration: InputDecoration(labelText: "Firstname"),
                onChanged: (text) => firstname = text,
              ),
              TextFormField(
                validator: (text) {
                  if(text!.isEmpty) {
                    return 'this field is required';
                  }
                  else {
                    return null;
                  }
                },
                decoration: InputDecoration(labelText: "Lastname"),
                onChanged: (text) => lastname = text,
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Text(
                    'Birth Day',
                    style: TextStyle(fontSize: 17),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  InkWell(
                      onTap: _showDatePicker,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        padding: EdgeInsets.all(5),
                        child: Text(
                          birthday.toString().replaceAll('00:00:00.000', ''),
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      )),
                ],
              ),
              TextFormField(
                validator: (text) {
                  if (state is NotValidePhone) {
                    return 'Please enter valide phone number';
                  }
                  else if(text!.isEmpty) {
                    return 'this field is required';
                  }
                  else if(state is ValidePhone) {
                    return null;
                  }
                },
                decoration: InputDecoration(labelText: "Phone"),
                onChanged: (text) {
                  context.read<CustomerCubit>().validatePhone(text);
                  registerFormKey.currentState!.validate();
                  phone = text;
                },
              ),
              TextFormField(
                validator: (text) {
                  if (state is NotValideEmail) {
                    return 'Please enter valide email address';
                  }
                  else if(text!.isEmpty) {
                    return 'this field is required';
                  }
                  else if(state is ValideEmail) {
                    return null;
                  }
                },
                decoration: InputDecoration(labelText: "Email"),
                onChanged: (text) {
                  context.read<CustomerCubit>().validateEmail(text);
                  registerFormKey.currentState!.validate();
                  email = text;
                },
              ),
              TextFormField(
                validator: (text) {
                  if (state is NotValideAccountNumber) {
                    return 'Please enter valide account number';
                  }
                  else if(text!.isEmpty) {
                    return 'this field is required';
                  }
                  else if(state is ValideAccountNumber) {
                    return null;
                  }
                },
                decoration: InputDecoration(labelText: "BankAccount"),
                onChanged: (text) {
                  context.read<CustomerCubit>().validateAccountNumber(text);
                  registerFormKey.currentState!.validate();
                  bankAccountNumber = text;
                },
              ),
              SizedBox(
                height: 50,
              ),
              (state is Loading)
                  ? LoadingWidget()
                  : ElevatedButton(onPressed: onSubmit, child: Text('Submit')),
            ]);
          },
        ),
      )),
    );
  }
}
