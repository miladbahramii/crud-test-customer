import 'package:dartz/dartz.dart';
import 'package:mc_crud_test/core/error/failures.dart';
import 'package:mc_crud_test/features/customer/domain/entities/customer.dart';

abstract class CustomerRepository {
  Future<Either<Failure, int>> createCustomer(
      {required String firstname,
      required String lastname,
      required DateTime birthDay,
      required String phone,
      required String email,
      required String bankAccountNumber});
}
