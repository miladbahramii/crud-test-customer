import 'package:mc_crud_test/features/customer/domain/entities/customer.dart';
import 'package:mc_crud_test/features/customer/domain/repositories/customer_repository.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';

class CreateCustomer implements UseCase<int, CreateCustomerParams> {
  CreateCustomer(this.repository);
  final CustomerRepository repository;
  @override
  Future<Either<Failure, int>> call(CreateCustomerParams params) async {
    return await repository.createCustomer(
        firstname: params.firstname,
        lastname: params.lastname,
        birthDay: params.birthDay,
        phone: params.phone,
        email: params.email,
        bankAccountNumber: params.bankAccountNumber);
  }
}

class CreateCustomerParams {
  final String firstname;
  final String lastname;
  final DateTime birthDay;
  final String phone;
  final String email;
  final String bankAccountNumber;

  CreateCustomerParams(
      {required this.firstname,
      required this.lastname,
      required this.birthDay,
      required this.phone,
      required this.email,
      required this.bankAccountNumber});
}
