
import 'package:equatable/equatable.dart';

class Customer extends Equatable  {
  final String firstname;
  final String lastname;
  final DateTime birthDay;
  final String phone;
  final String email;
  final String bankAccountNumber;

  Customer(
      {required this.firstname,
      required this.lastname,
      required this.birthDay,
      required this.phone,
      required this.email,
      required this.bankAccountNumber});

  @override
  List<Object> get props {
    return [
      firstname,
      lastname,
      birthDay,
      phone,
      email,
      bankAccountNumber,
    ];
  }
}
