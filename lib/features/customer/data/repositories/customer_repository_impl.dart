import 'package:drift/drift.dart';
import 'package:mc_crud_test/features/customer/data/datasources/customer_local_data_source.dart';
import 'package:mc_crud_test/features/customer/data/models/customer_model.dart';
import 'package:mc_crud_test/features/customer/domain/entities/customer.dart';
import 'package:mc_crud_test/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:mc_crud_test/features/customer/domain/repositories/customer_repository.dart';

import '../../../../core/error/exeptions.dart';

class CustomerRepositoryImpl implements CustomerRepository {
  final CustomerLocalDataSource localDataSource;

  CustomerRepositoryImpl({required this.localDataSource});

  @override
  Future<Either<Failure, int>> createCustomer(
      {required String firstname,
      required String lastname,
      required DateTime birthDay,
      required String phone,
      required String email,
      required String bankAccountNumber}) async {
    try {
      final CustomerModel = await localDataSource.createCustomer(
          CustomerModelCompanion(
              firstname: Value(firstname),
              lastname: Value(lastname),
              birthDay: Value(birthDay),
              phone: Value(phone),
              email: Value(email),
              bankAccountNumber: Value(bankAccountNumber)));
      return Right(CustomerModel);
    } on UniqueConstrantExeption catch (e) {
      return Left(UniqueConstrantFailure(e.message));
    } on CacheException {
      return Left(CacheFailure());
    }
  }
}
