import 'package:drift/native.dart';
import 'package:mc_crud_test/core/error/exeptions.dart';
import 'package:mc_crud_test/features/customer/data/models/customer_model.dart';

abstract class CustomerLocalDataSource {
  Future<int> createCustomer(CustomerModelCompanion customer);
}

class CustomerLocalDataSourceImpl implements CustomerLocalDataSource {
  final AppDatabase database;

  CustomerLocalDataSourceImpl({required this.database});

  @override
  Future<int> createCustomer(CustomerModelCompanion customer) async {
    try {
      final customerQuery = await database.addCustomer(customer);
      final customers = await database.allCustomersEntries;
      return customerQuery;
    } catch (e) {
      if(e is SqliteException) {
        switch (e.extendedResultCode) {
          case 2067:
            throw UniqueConstrantExeption(e.message);
          default:
            throw CacheException();
        }
      }
      throw UnExpectedExeption();
    }
  }
}