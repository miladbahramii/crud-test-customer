import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'dart:io';

part 'customer_model.g.dart';

class CustomerModel extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get firstname => text().withLength(min: 1, max: 32)();
  TextColumn get lastname => text().withLength(min: 1, max: 32)();
  DateTimeColumn get birthDay => dateTime().nullable()();
  TextColumn get phone => text().customConstraint('NOT NULL UNIQUE')();
  TextColumn get email => text().customConstraint('NOT NULL UNIQUE')();
  TextColumn get bankAccountNumber => text()();
}

LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'db.sqlite'));
    return NativeDatabase(file);
  });
}

@DriftDatabase(tables: [CustomerModel])
class AppDatabase extends _$AppDatabase {
  // we tell the database where to store the data with this constructor
  AppDatabase() : super(_openConnection());

  // loads all Customer entries
  Future<List<CustomerModelData>> get allCustomersEntries =>
      select(customerModel).get();

  Future<int> addCustomer(CustomerModelCompanion entry) {
    return into(customerModel).insert(entry);
  }

  @override
  int get schemaVersion => 1;
}
