// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_model.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class CustomerModelData extends DataClass
    implements Insertable<CustomerModelData> {
  final int id;
  final String firstname;
  final String lastname;
  final DateTime? birthDay;
  final String phone;
  final String email;
  final String bankAccountNumber;
  CustomerModelData(
      {required this.id,
      required this.firstname,
      required this.lastname,
      this.birthDay,
      required this.phone,
      required this.email,
      required this.bankAccountNumber});
  factory CustomerModelData.fromData(Map<String, dynamic> data,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return CustomerModelData(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      firstname: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}firstname'])!,
      lastname: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}lastname'])!,
      birthDay: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}birth_day']),
      phone: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}phone'])!,
      email: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}email'])!,
      bankAccountNumber: const StringType().mapFromDatabaseResponse(
          data['${effectivePrefix}bank_account_number'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['firstname'] = Variable<String>(firstname);
    map['lastname'] = Variable<String>(lastname);
    if (!nullToAbsent || birthDay != null) {
      map['birth_day'] = Variable<DateTime?>(birthDay);
    }
    map['phone'] = Variable<String>(phone);
    map['email'] = Variable<String>(email);
    map['bank_account_number'] = Variable<String>(bankAccountNumber);
    return map;
  }

  CustomerModelCompanion toCompanion(bool nullToAbsent) {
    return CustomerModelCompanion(
      id: Value(id),
      firstname: Value(firstname),
      lastname: Value(lastname),
      birthDay: birthDay == null && nullToAbsent
          ? const Value.absent()
          : Value(birthDay),
      phone: Value(phone),
      email: Value(email),
      bankAccountNumber: Value(bankAccountNumber),
    );
  }

  factory CustomerModelData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CustomerModelData(
      id: serializer.fromJson<int>(json['id']),
      firstname: serializer.fromJson<String>(json['firstname']),
      lastname: serializer.fromJson<String>(json['lastname']),
      birthDay: serializer.fromJson<DateTime?>(json['birthDay']),
      phone: serializer.fromJson<String>(json['phone']),
      email: serializer.fromJson<String>(json['email']),
      bankAccountNumber: serializer.fromJson<String>(json['bankAccountNumber']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'firstname': serializer.toJson<String>(firstname),
      'lastname': serializer.toJson<String>(lastname),
      'birthDay': serializer.toJson<DateTime?>(birthDay),
      'phone': serializer.toJson<String>(phone),
      'email': serializer.toJson<String>(email),
      'bankAccountNumber': serializer.toJson<String>(bankAccountNumber),
    };
  }

  CustomerModelData copyWith(
          {int? id,
          String? firstname,
          String? lastname,
          DateTime? birthDay,
          String? phone,
          String? email,
          String? bankAccountNumber}) =>
      CustomerModelData(
        id: id ?? this.id,
        firstname: firstname ?? this.firstname,
        lastname: lastname ?? this.lastname,
        birthDay: birthDay ?? this.birthDay,
        phone: phone ?? this.phone,
        email: email ?? this.email,
        bankAccountNumber: bankAccountNumber ?? this.bankAccountNumber,
      );
  @override
  String toString() {
    return (StringBuffer('CustomerModelData(')
          ..write('id: $id, ')
          ..write('firstname: $firstname, ')
          ..write('lastname: $lastname, ')
          ..write('birthDay: $birthDay, ')
          ..write('phone: $phone, ')
          ..write('email: $email, ')
          ..write('bankAccountNumber: $bankAccountNumber')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id, firstname, lastname, birthDay, phone, email, bankAccountNumber);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CustomerModelData &&
          other.id == this.id &&
          other.firstname == this.firstname &&
          other.lastname == this.lastname &&
          other.birthDay == this.birthDay &&
          other.phone == this.phone &&
          other.email == this.email &&
          other.bankAccountNumber == this.bankAccountNumber);
}

class CustomerModelCompanion extends UpdateCompanion<CustomerModelData> {
  final Value<int> id;
  final Value<String> firstname;
  final Value<String> lastname;
  final Value<DateTime?> birthDay;
  final Value<String> phone;
  final Value<String> email;
  final Value<String> bankAccountNumber;
  const CustomerModelCompanion({
    this.id = const Value.absent(),
    this.firstname = const Value.absent(),
    this.lastname = const Value.absent(),
    this.birthDay = const Value.absent(),
    this.phone = const Value.absent(),
    this.email = const Value.absent(),
    this.bankAccountNumber = const Value.absent(),
  });
  CustomerModelCompanion.insert({
    this.id = const Value.absent(),
    required String firstname,
    required String lastname,
    this.birthDay = const Value.absent(),
    required String phone,
    required String email,
    required String bankAccountNumber,
  })  : firstname = Value(firstname),
        lastname = Value(lastname),
        phone = Value(phone),
        email = Value(email),
        bankAccountNumber = Value(bankAccountNumber);
  static Insertable<CustomerModelData> custom({
    Expression<int>? id,
    Expression<String>? firstname,
    Expression<String>? lastname,
    Expression<DateTime?>? birthDay,
    Expression<String>? phone,
    Expression<String>? email,
    Expression<String>? bankAccountNumber,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (firstname != null) 'firstname': firstname,
      if (lastname != null) 'lastname': lastname,
      if (birthDay != null) 'birth_day': birthDay,
      if (phone != null) 'phone': phone,
      if (email != null) 'email': email,
      if (bankAccountNumber != null) 'bank_account_number': bankAccountNumber,
    });
  }

  CustomerModelCompanion copyWith(
      {Value<int>? id,
      Value<String>? firstname,
      Value<String>? lastname,
      Value<DateTime?>? birthDay,
      Value<String>? phone,
      Value<String>? email,
      Value<String>? bankAccountNumber}) {
    return CustomerModelCompanion(
      id: id ?? this.id,
      firstname: firstname ?? this.firstname,
      lastname: lastname ?? this.lastname,
      birthDay: birthDay ?? this.birthDay,
      phone: phone ?? this.phone,
      email: email ?? this.email,
      bankAccountNumber: bankAccountNumber ?? this.bankAccountNumber,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (firstname.present) {
      map['firstname'] = Variable<String>(firstname.value);
    }
    if (lastname.present) {
      map['lastname'] = Variable<String>(lastname.value);
    }
    if (birthDay.present) {
      map['birth_day'] = Variable<DateTime?>(birthDay.value);
    }
    if (phone.present) {
      map['phone'] = Variable<String>(phone.value);
    }
    if (email.present) {
      map['email'] = Variable<String>(email.value);
    }
    if (bankAccountNumber.present) {
      map['bank_account_number'] = Variable<String>(bankAccountNumber.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CustomerModelCompanion(')
          ..write('id: $id, ')
          ..write('firstname: $firstname, ')
          ..write('lastname: $lastname, ')
          ..write('birthDay: $birthDay, ')
          ..write('phone: $phone, ')
          ..write('email: $email, ')
          ..write('bankAccountNumber: $bankAccountNumber')
          ..write(')'))
        .toString();
  }
}

class $CustomerModelTable extends CustomerModel
    with TableInfo<$CustomerModelTable, CustomerModelData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $CustomerModelTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _firstnameMeta = const VerificationMeta('firstname');
  @override
  late final GeneratedColumn<String?> firstname = GeneratedColumn<String?>(
      'firstname', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 32),
      type: const StringType(),
      requiredDuringInsert: true);
  final VerificationMeta _lastnameMeta = const VerificationMeta('lastname');
  @override
  late final GeneratedColumn<String?> lastname = GeneratedColumn<String?>(
      'lastname', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 32),
      type: const StringType(),
      requiredDuringInsert: true);
  final VerificationMeta _birthDayMeta = const VerificationMeta('birthDay');
  @override
  late final GeneratedColumn<DateTime?> birthDay = GeneratedColumn<DateTime?>(
      'birth_day', aliasedName, true,
      type: const IntType(), requiredDuringInsert: false);
  final VerificationMeta _phoneMeta = const VerificationMeta('phone');
  @override
  late final GeneratedColumn<String?> phone = GeneratedColumn<String?>(
      'phone', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL UNIQUE');
  final VerificationMeta _emailMeta = const VerificationMeta('email');
  @override
  late final GeneratedColumn<String?> email = GeneratedColumn<String?>(
      'email', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: true,
      $customConstraints: 'NOT NULL UNIQUE');
  final VerificationMeta _bankAccountNumberMeta =
      const VerificationMeta('bankAccountNumber');
  @override
  late final GeneratedColumn<String?> bankAccountNumber =
      GeneratedColumn<String?>('bank_account_number', aliasedName, false,
          type: const StringType(), requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [id, firstname, lastname, birthDay, phone, email, bankAccountNumber];
  @override
  String get aliasedName => _alias ?? 'customer_model';
  @override
  String get actualTableName => 'customer_model';
  @override
  VerificationContext validateIntegrity(Insertable<CustomerModelData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('firstname')) {
      context.handle(_firstnameMeta,
          firstname.isAcceptableOrUnknown(data['firstname']!, _firstnameMeta));
    } else if (isInserting) {
      context.missing(_firstnameMeta);
    }
    if (data.containsKey('lastname')) {
      context.handle(_lastnameMeta,
          lastname.isAcceptableOrUnknown(data['lastname']!, _lastnameMeta));
    } else if (isInserting) {
      context.missing(_lastnameMeta);
    }
    if (data.containsKey('birth_day')) {
      context.handle(_birthDayMeta,
          birthDay.isAcceptableOrUnknown(data['birth_day']!, _birthDayMeta));
    }
    if (data.containsKey('phone')) {
      context.handle(
          _phoneMeta, phone.isAcceptableOrUnknown(data['phone']!, _phoneMeta));
    } else if (isInserting) {
      context.missing(_phoneMeta);
    }
    if (data.containsKey('email')) {
      context.handle(
          _emailMeta, email.isAcceptableOrUnknown(data['email']!, _emailMeta));
    } else if (isInserting) {
      context.missing(_emailMeta);
    }
    if (data.containsKey('bank_account_number')) {
      context.handle(
          _bankAccountNumberMeta,
          bankAccountNumber.isAcceptableOrUnknown(
              data['bank_account_number']!, _bankAccountNumberMeta));
    } else if (isInserting) {
      context.missing(_bankAccountNumberMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  CustomerModelData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return CustomerModelData.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $CustomerModelTable createAlias(String alias) {
    return $CustomerModelTable(attachedDatabase, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  late final $CustomerModelTable customerModel = $CustomerModelTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [customerModel];
}
