# CRUD Code Test

This project imeplemented on the Clean Architecture.

```
Customer {
	Firstname
	Lastname
	DateOfBirth
	PhoneNumber
	Email
	BankAccountNumber
}
```

## Clean Architecture Diagram

<img src="./clean_architecture_diagram.png" max-width="250px" max-height="250px" />

### Validations
- implemented a Validator service for validate Phone, Email and BankAccount

### Storage 
- Storage service is Moor, its a extention of SQLITE

